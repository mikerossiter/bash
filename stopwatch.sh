#!/bin/bash

# Check if figlet is installed
if ! command -v figlet &> /dev/null
then
    echo "figlet is not installed. Please install figlet and try again."
    exit 1
fi

start=$(date +%s)

while true; do
    time="$(($(date +%s) - $start))"
    elapsed_time="$(date -u -d @$time +%H:%M:%S)"
    printf "Ctrl+C to quit\n"
    figlet "$elapsed_time"
    sleep 1
    clear
done

