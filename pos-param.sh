#!/bin/bash

# Script to test positional parameters i.e. ./pos-param.sh arg1 arg2
echo "The script was called with $# arguments: $@"
